type alg = RS256 | HS256 [@@deriving encoding {upper}, jsoo]

type header = {
  alg: alg;
  kid: string option;
} [@@deriving encoding, jsoo]

let header_enc =
  Json_encoding.(
    conv (fun h -> h, ((), ())) (fun (h, ((), ())) -> h) @@
    merge_objs header_enc @@ merge_objs (obj1 (req "typ" (constant "JWT"))) unit)

[@@@jsoo
  class type header_with_typ_jsoo = object
    inherit header_jsoo
    method typ: Ezjs_min.js_string Ezjs_min.t Ezjs_min.readonly_prop
  end
  let header_to_jsoo h : header_with_typ_jsoo Ezjs_min.t =
    let js = header_to_jsoo h in
    object%js
      val alg = js##.alg
      val kid = js##.kid
      val typ = Ezjs_min.string "JWT"
    end
  let header_of_jsoo (js: header_with_typ_jsoo Ezjs_min.t) =
    header_of_jsoo (Ezjs_min.Unsafe.coerce js)
]

let aud_enc = Json_encoding.(union [
  case (list string) Option.some Fun.id;
  case string (function [x] -> Some x | _ -> None) (fun s -> [s])
])

type payload = {
  iss: string option;
  sub: string option;
  aud: string list; [@dft []] [@encoding aud_enc]
  exp: float option;
  nbf: float option;
  iat: float option;
  jti: string option;
} [@@deriving encoding {ignore}, jsoo {remove_undefined}]

type jwk = {
  alg: alg;
  kty: [`RSA];
  n: string;
  e: string;
  use: [`sign [@key "sig"] | `enc] option;
  kid: string option;
} [@@deriving encoding {ignore}, jsoo {remove_undefined}]

type jwks = (jwk list [@wrap "keys"]) [@@deriving encoding]

type jwt = {
  header: (header [@class_type header_with_typ_jsoo]);
  payload: payload;
  signature: string option;
} [@@deriving encoding, jsoo]

type error = [
  | `Http of int * string
  | `Msg of string
  | `Exn of exn
  | `Wrong of jwt
]

let pp_error fmt (e: error) = match e with
  | `Http (code, content) -> Format.fprintf fmt "http error %d: %s" code content
  | `Msg s -> Format.fprintf fmt "error: %s" s
  | `Exn exn -> Json_encoding.print_error ?print_unknown:None fmt exn
  | `Wrong jwt -> Format.fprintf fmt "wrong jwt:\n%s" (EzEncoding.construct jwt_enc jwt)

let alphabet = Base64.uri_safe_alphabet
let b64dec s = Base64.decode_exn ~alphabet ~pad:false s

open Mirage_crypto_pk

let pub ~n ~e : (_, [> error ]) result =
  let n = Z_extra.of_octets_be @@ b64dec n in
  let e = Z_extra.of_octets_be @@ b64dec e in
  Rsa.pub ~e ~n

let (let>) = Lwt.bind
let (let>?) x f = Lwt.bind x (function Error e -> Lwt.return_error e | Ok x -> f x)

let get_jwk ~kid path : (_, [> error ]) result Lwt.t =
  let> r = EzReq_lwt.get (EzAPI.URL path) in
  match r with
  | Error (code, content) -> Lwt.return_error (`Http (code, Option.value ~default:"" content))
  | Ok s ->
    let jwks = EzEncoding.destruct jwks_enc s in
    match List.find_opt (fun jwk -> jwk.kid = Some kid) jwks with
    | Some k -> Lwt.return_ok k
    | None -> Lwt.return_error (`Msg "kid not found in jwks")

let jwk_cache = Hashtbl.create 10

let get_key ?n ?e ?kid ?iss ?(cache=true)
    ?(path=fun iss -> Filename.concat iss ".well-known/jwks.json")  () = match n, e, kid, iss with
  | Some n, Some e, _, _ -> Lwt.return (pub ~n ~e)
  | _, _, Some kid, Some iss ->
    let path = path iss in
    let>? jwk = if cache then
        match Hashtbl.find_opt jwk_cache (path, kid) with
        | None ->
          let>? jwk = get_jwk ~kid path in
          Hashtbl.add jwk_cache (path, kid) jwk;
          Lwt.return_ok jwk
        | Some jwk -> Lwt.return_ok jwk
      else get_jwk ~kid path in
    Lwt.return (pub ~n:jwk.n ~e:jwk.e)
  | _ -> Lwt.return_error (`Msg "either ('n' and 'e') or ('kid' and 'iss') required for JWK pubkey")

let get_payload_aux ?data ?(debug=0) p64 =
  let p = b64dec p64 in
  if debug >= 1 then Format.printf "[jwt] payload: %s@." p;
  match data with
  | None -> EzEncoding.destruct payload_enc p, None
  | Some enc ->
    let p, data = EzEncoding.destruct (Json_encoding.merge_objs payload_enc enc) p in
    p, Some data

let get_payload ?data ?debug t =
  match String.split_on_char '.' t with
  | [ _; p64; _ ] -> Ok (get_payload_aux ?debug ?data p64)
  | _ -> Error (`Msg "wrong JWT format")

let verify ?(debug=0) ?n ?e ?jwk ?path ?(data: 'a Json_encoding.encoding option) t : ((payload * 'a option), error) result Lwt.t =
  let process () =
    match String.split_on_char '.' t with
    | [ h64; p64; s64 ] ->
      let signature = b64dec s64 in
      let h = b64dec h64 in
      if debug >= 2 then Format.printf "[jwt] header: %s@." h;
      let header = EzEncoding.destruct header_enc h in
      let payload, data = get_payload_aux ~debug ?data p64 in
      let>? key = match jwk, n, e with
        | Some {n; e; _}, _, _ | _, Some n, Some e -> get_key ~n ~e ()
        | _ -> get_key ?kid:header.kid ?iss:payload.iss ?path () in
      let b = Rsa.PKCS1.verify ~key ~signature ~hashp:(fun _ -> true) (`Digest (Digestif.SHA256.(to_raw_string @@ digest_string (h64 ^ "." ^ p64)))) in
      if b then Lwt.return_ok (payload, data)
      else Lwt.return_error (`Wrong { payload; header; signature = Some signature })
    | _ -> Lwt.return_error (`Msg "wrong JWT format") in
  Lwt.catch process (fun exn -> Lwt.return_error (`Exn exn))
