open Jwt

let path iss = Filename.concat iss "jwt/jwks.json"

let () =
  Lwt_main.run @@
  let> r = verify ~debug:2 ~path Sys.argv.(1) in
  let () = match r with
    | Ok (payload, _data) ->
      Format.printf "%s@." (EzEncoding.construct ~compact:false payload_enc payload)
    | Error e -> Format.printf "%a@." pp_error e; exit 1 in
  Lwt.return_unit
